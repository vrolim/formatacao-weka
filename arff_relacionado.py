def abre(entrada, saida):
    with open(saida, "w") as s:
        #     s.write("@relation " + entrada + "\n\n")
        #     s.write("@attribute 'velocidade' real \n")
        #     s.write("@attribute 'rpm' {'?','baixo', 'medio', 'alto'} \n")
        #     s.write("@attribute 'carga_motor' real \n")
        #     s.write("@attribute 'temperatura_ar' {'?','calor', 'medio', 'frio'}\n")
        #     s.write("@attribute 'posicao_pedal'  real \n")
        #     s.write("@attribute 'acx' real \n")
        #     s.write("@attribute 'acy' real \n")
        #     s.write("@attribute 'acz' real \n")
        #     s.write("@attribute 'conforto' {'?','confortavel', 'desconfortavel'}\n\n")
        #     s.write("@data\n")

        with open(entrada, 'r') as arq:
            conjunto = []
            velocidade = []
            rpm = []
            carga_motor = []
            temperatura_ar = []
            posicao_pedal = []
            acx = []
            acy = []
            acz = []
            for linha in arq:
                l = linha.split("\t")
                if len(l[17]) > 1:
                    
                    s.write(','.join(conjunto))
                    s.write(','.join(velocidade))
                    s.write(','.join(rpm))
                    s.write(','.join(carga_motor))
                    s.write(','.join(temperatura_ar))
                    s.write(','.join(posicao_pedal))
                    s.write(','.join(acx))
                    s.write(','.join(acy))
                    s.write(','.join(acz))

                    s.write('\n')
                    conjunto = [l[17], l[18]]

                if len(conjunto) > 0:
                    if len(l[5]) > 1:
                        velocidade.append(l[5])

                    if len(l[6]) > 1:
                        rpm.append(l[6])

                    if len(l[7]) > 1:
                        carga_motor.append(l[7])

                    if len(l[9]) > 1:
                        temperatura_ar.append(l[9])

                    if len(l[13]) > 1:
                        posicao_pedal.append(l[13])

                    if len(l[26]) > 1:
                        acx.append(l[26])

                    if len(l[27]) > 1:
                        acy.append(l[27])

                    if len(l[28]) > 1:
                        acz.append(l[28])
