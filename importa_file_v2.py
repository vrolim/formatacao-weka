def vel():
    vels = range(0, 100)
    ret = []
    for v in vels:
        ret.append(("'" + str(v) + "',"))

    ret.append("'"+str(100)+"'")
    return ''.join(ret)


def giro(valor):
    if valor == " " or valor == "" or valor == "	" or valor == "\n":
        return ""
    valor = float(valor)
    if valor < 1000:
        return "'baixo'"
    elif valor >= 100 and valor <= 1500:
        return "'medio'"
    return "'alto'"


def temp(valor):
    if valor == " " or valor == "" or valor == "	" or valor =="\n":
        return ""
    valor = float(valor)
    if valor > 25:
        return "'calor'"
    elif valor < 18:
        return "'frio'"
    return "'medio'"


def conforto(valor):
    if valor == " " or valor == "" or valor == "	" or valor == "\n":
        return ""
    valor = float(valor)
    if valor > 1.18:
        return "'desconfortavel'"
    return "'confortavel'"


def arredonda(valor):
    if valor == " " or valor == "" or valor == "	" or valor == "\n":
        return ""
    return str(round(float(valor)))


def abre(entrada, saida):
    with open(saida, "w") as s:
        s.write("@relation " + entrada + "\n\n")
        s.write("@attribute 'velocidade' {'?'," + vel() + "}\n")
        s.write("@attribute 'rpm' {'?','baixo', 'medio', 'alto'} \n")
        s.write("@attribute 'carga_motor' {'?'," + vel() + "}\n")
        s.write("@attribute 'posicao_pedal'  {'?'," + vel() + "}\n")
        s.write("@attribute 'velocidade_gps' {'?'," + vel() + "}\n")
        s.write("@attribute 'acx' {'?', 'confortavel', 'desconfortavel'}\n")
        s.write("@attribute 'acy' {'?', 'confortavel', 'desconfortavel'}\n")
        s.write("@attribute 'acz' {'?', 'confortavel', 'desconfortavel'}\n")
        s.write("@data\n")

        with open(entrada, 'r') as arq:
            for linha in arq:
                l = linha.split("\t")
                # data = "?"
                # if l[15] != "":
                #     data = "'20" + l[15] + " " + l[16] + "'"

                dados = [arredonda(l[5]), giro(l[6]), arredonda(l[7]), arredonda(l[13]),
                         arredonda(l[20]), conforto(l[26]), conforto(l[27]), conforto(l[28])]
                if dados[-1] == "\n":
                    dados[-1] = '0\n'
                i = 0
                while i < len(dados):
                    if dados[i] == "":
                        dados[i] = "'?'"
                    i += 1
                escreve = ','.join(dados)
                s.write(escreve)
                s.write('\n')

        return s
